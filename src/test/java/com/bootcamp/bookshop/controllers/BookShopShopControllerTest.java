package com.bootcamp.bookshop.controllers;

import com.bootcamp.bookshop.models.BookShop;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookShopShopControllerTest {
    private BookShopController bookShopController;

    @Before
    public void setup() {
        bookShopController = new BookShopController();
    }

    @Test
    public void shouldReturnBookShopNameOnGetRequest() {
        assertEquals(new BookShop("Shreeghanesh and Shubham"), bookShopController.getBook());
    }
}
