package com.bootcamp.bookshop.models;


import java.util.Objects;

public class BookShop {

    private String name;

    public BookShop(String bookName) {
        this.name = bookName;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookShop bookShop = (BookShop) o;
        return Objects.equals(name, bookShop.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
