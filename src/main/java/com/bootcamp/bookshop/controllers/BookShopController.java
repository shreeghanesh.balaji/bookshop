package com.bootcamp.bookshop.controllers;

import com.bootcamp.bookshop.models.BookShop;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookShopController {

    @GetMapping("/book")
    public BookShop getBook() {
        return new BookShop("Shreeghanesh and Shubham");
    }
}
