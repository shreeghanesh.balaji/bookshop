package com.bootcamp.bookshop.integration;

import com.bootcamp.bookshop.controllers.BookShopController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookShopController.class, secure = false)
public class BookShopShopIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void verifyCorrectBookName() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/book").accept(
                MediaType.APPLICATION_JSON);
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        String expectedResult = "{\"name\": \"Shreeghanesh and Shubham\"}";

        JSONAssert.assertEquals(expectedResult, mvcResult.getResponse()
                .getContentAsString(), false);
    }
}
